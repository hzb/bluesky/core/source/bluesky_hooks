## Bluesky Hooks

Run plans before or after a specific motor is moved. See the example in `example/example_hooks.ipynb`

This work was inspired by the [MotionHooks in Bliss](https://bliss.gitlab-pages.esrf.fr/bliss/master/motion_hook.html#example-motor-with-air-pad), Sardana and Spec.

## To Install

```
python3 -m pip install git+https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bluesky_hooks.git
```

## To include this in a python file

```
from bluesky_hooks.hooks import MotionHook, MotionHooksPreprocessor
```

## How to define a hook

A hook must be a generator that yields messages. It can be any generator. It must have only one argument, `val` which is the value that the motor in question is going to be set to or has been set to
```
def example_hook(val):
        
        yield from bps.mov(motorA, val/2)
        yield from bps.mov(motorC, val/2)
```

Hooks can be defined before or after a motion. To define a hook use the `MotionHook` class. This class let's us specify:

- The motor we want to use runs plans before or after motion
- A plan to be run before the motion
- A plan to be run after the motion

In the example below a hook is defined that will be run whenever `motorB` is set. It will run the plan `example_hook` before the motor is moved, but nothing afterwards.

```

hookA  = MotionHook('hookA',motorB,example_hook,[])
```

In this case we run a hook only afterwards

```

hookB  = MotionHook('hookB',motorB,[],example_hook)
```

In this case, both before and afterwards

```

hookC  = MotionHook('hookC',motorB,example_hook,example_hook)
```

If you need to use another value than the one being written to a given motor, you can also do that

```
def example_motion_hook_with_read(val):
    
    ret = yield from bps.rd(motorD)
    yield from bps.mov(motorB, ret/0.5)
    yield from bps.mov(motorC, ret/0.3)
```
## How to make the RE run hooks every time it does anything

The syntax is similar to supplimental detectors
```
mh = MotionHooksPreprocessor()
RE = RunEngine({})
RE.preprocessors.append(mh)
```

We can then add motion hooks like this. It's a list.

```
mh.hooks = [hookA, hookB]
```
The hooks will be run in order from left to right. 

## Things to watch out for

If you include a motor inside your hook plan that is also the motor that will be used to trigger the hook, then you will make a recurssive plan that will move motors for ever. Don't do that, it will raise an error.

```
def do_not_do_this(val):
    
    yield from bps.mov(motorA, val/5)

hookA  = MotionHook('hookA',motorA,do_not_do_this)

mh.hooks = [hookA]
```

