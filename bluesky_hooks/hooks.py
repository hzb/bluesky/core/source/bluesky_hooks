class MotionHooksPreprocessor:
    """
    A configurable preprocessor for plans to be run before or after when a given motor is set

    This is a plan preprocessor. It inserts messages into plans to:

    * run a given plan before a given motor is moved
    * run a given plan after a given motor is moved

    Internally, it uses the plan preprocessors:

    * :func:`run_before_motion`
    * :func:`run_after_motion`

    Parameters
    ----------
    hooks : list
        A list of MotionHook objects which 
    monitors : list
        Signals (not multi-signal Devices) to be monitored during each run,
        generating readings asynchronously
    flyers : list
        "Flyable" Devices to be kicked off before each run and collected
        at the end of each run

    Examples
    --------
    Create an instance of SupplementalData and apply it to a RunEngine.

    >>> mh = MotionHooksPreprocessor(hooks=[hook1, hook2, hook3)
    >>> RE = RunEngine({})
    >>> RE.preprocessors.append(mh)

    """
    def __init__(self, *, hooks=None):
        if hooks is None:
            hooks = []
       
        self.hooks = list(hooks)

    def __repr__(self):
        return ("{cls}(baseline={hooks}").format(cls=type(self).__name__, **vars(self))

    def __call__(self, plan):
        """
        Insert messages into a plan.

        Parameters
        ----------
        plan : iterable or iterator
            a generator, list, or similar containing `Msg` objects
        """
        # Read this as going from the inside out
        for hook in self.hooks:
            plan = hook.preprocessor(plan)
       
        return (yield from plan)
    

#from bessyii.default_detectors import bessy_plan_mutator as plan_mutator
from bluesky.preprocessors import plan_mutator
from bluesky.preprocessors import (
    
    single_gen,
    ensure_generator,
    inject_md_wrapper,
    stage_wrapper
    
)


    
class MotionHook:

    """
    Parameters
    ----------
    motor : a settable object
        The device which we want to run the hook before or after at
    pre_plan : a generator 
        the plan to be run before the motion of motor
    post_plan : a generator
        the plan to be run after the motion of motor
    """

    def __init__(self,name, motor,pre_plan=None, post_plan=None):
       
        self.name = name
        self.motor = motor
        self.pre_plan = pre_plan
        self.post_plan = post_plan
        self.wait_key = None
        self.set_val = None
        
        # Validate the plans
        self.plan_validation(self.pre_plan)
        self.plan_validation(self.post_plan)
        
    def plan_validation(self, plan, test_val=0):
        """
        check that the plan doesn't contain the trigger motor, which would a recursive plan
        """
        trigger_motor_name = self.motor.name
        
        if plan != [] and plan!= None:
            for message in plan(test_val):
                if message.command == 'set':
                    if message.obj.name == trigger_motor_name:
                        raise ValueError(f"The motor '{self.motor.name}' which is monitored to trigger the motion hook '{self.name}' is also used in one of the plans in the hook. This will cause a recursive plan")
        
    

    def insert_before_set(self, msg):
            
        if msg.command == 'set' and msg.obj.name == self.motor.name and self.pre_plan:

            def new_gen():
                yield from self.pre_plan(msg.args[0])
                yield msg
            return new_gen(), None
        else:
            return None, None

    def insert_after_wait(self, msg):

        # Find the wait group of the set message
        if msg.command == 'set'  and msg.obj.name == self.motor.name and self.post_plan:
            self.set_val = msg.args[0]
            self.wait_key = msg.kwargs['group']
       
        # When we find a wait message, check if it has the wait key we are looking for
        if msg.command == 'wait'  and self.wait_key and self.post_plan:

            if msg.kwargs['group'] == self.wait_key:
                def new_gen():
                    yield from self.post_plan(self.set_val)
                self.wait_key = None
                return single_gen(msg), new_gen()
            
            else:
                return None, None
        else:
            return None, None


    def preprocessor(self, plan):
        
        plan = plan_mutator(plan, self.insert_before_set)
        plan = plan_mutator(plan, self.insert_after_wait)
        self.wait_key = None
        self.set_val = None
        return plan