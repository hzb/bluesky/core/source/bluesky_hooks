import pytest
from ophyd.sim import SynAxis
import bluesky.plan_stubs as bps
from bluesky import RunEngine
from bluesky_hooks.hooks import MotionHook, MotionHooksPreprocessor

motorA = SynAxis(name='motorA')
motorB = SynAxis(name='motorB')
motorC = SynAxis(name='motorC')

def example_hook_A(val):
    ret = yield from bps.rd(motorA)
    yield from bps.mov(motorB, ret/5)
    yield from bps.mov(motorC, ret/3)
    
def example_hook_condition(val):
    if val >= 1:
        yield from bps.mov(motorB, 1)
        yield from bps.mov(motorC, 0)


def test_no_hooks():
    # Test that if no hooks are added, we add no messages
    hook  = MotionHook('hook',motorB)
    plan = bps.mov(motorB, 10)
    processed = hook.preprocessor(bps.mov(motorB, 10))

    assert sum(1 for message in plan)==2
    assert sum(1 for message in processed)==2

def test_pre_hook():
    #Test for pre_hooks being added
    hook  = MotionHook('hook',motorA, example_hook_A)
    plan = bps.mov(motorA, 10)
    processed = hook.preprocessor(bps.mov(motorA, 10))

    assert sum(1 for message in plan)==2
    #2 from initial move, plus one read, plus 2 move and wait
    assert sum(1 for message in processed)==7
    
def test_post_hook():
    #Test for pre_hooks being added
    hook  = MotionHook('hook',motorA, [],example_hook_A)
    plan = bps.mov(motorA, 10)
    processed = hook.preprocessor(bps.mov(motorA, 10))

    assert sum(1 for message in plan)==2
    #2 from initial move, plus one read, plus 2 move and wait
    assert sum(1 for message in processed)==7

def test_pre_and_post_hook():
    #Test for pre_hooks being added
    hook  = MotionHook('hook',motorA, example_hook_A,example_hook_A)
    plan = bps.mov(motorA, 10)
    processed = hook.preprocessor(bps.mov(motorA, 10))

    assert sum(1 for message in plan)==2
    #2 from initial move, plus (one read, plus 2 move and wait) x2
    assert sum(1 for message in processed)==12

def test_hook_no_motor():
    #Test for no hooks added if it's not the correct motor
    hook  = MotionHook('hook',motorA, [],example_hook_A)
    plan = bps.mov(motorA, 10)
    processed = hook.preprocessor(bps.mov(motorB, 10))

    assert sum(1 for message in plan)==2
    assert sum(1 for message in processed)==2
    
def test_hook_plan_validator():
    #Test that an error is raised when the trigger motor is also used in a plan
     with pytest.raises(Exception):
        hook  = MotionHook('hook',motorB, [],example_hook_A)
 



def test_motion_hooks_preprocessor():
    #Test for for pre_hook addes
    hookA  = MotionHook('hook',motorA, [],example_hook_A)
    mh = MotionHooksPreprocessor(hooks=[hookA])
    RE=RunEngine({})
    RE.preprocessors.append(mh)

    RE(bps.mov(motorA, 15))

    assert motorA.read()['motorA']['value'] == 15.0
    assert motorB.read()['motorB']['value'] == 3.0
    assert motorC.read()['motorC']['value'] == 5.0
    
def test_motion_hooks_preprocessor_with_val():

    def example_hook_A2(val):
        
        yield from bps.mov(motorB, val/4)
        yield from bps.mov(motorC, val/3)

    #Test for no hooks added if it's not the correct motor
    hookA  = MotionHook('hookA',motorA,example_hook_A2)
    mh = MotionHooksPreprocessor(hooks=[hookA])
    RE=RunEngine({})
    RE.preprocessors.append(mh)

    RE(bps.mov(motorA, 12))

    assert motorA.read()['motorA']['value'] == 12.0
    assert motorB.read()['motorB']['value'] == 3.0
    assert motorC.read()['motorC']['value'] == 4.0


def test_motion_hooks_preprocessor_multiple():
    # Test that I can add multiple hooks that will be run in order

    def example_pre_hook_A2(val):
        
        yield from bps.mov(motorB, val/4)
        yield from bps.mov(motorC, val/3)

    def example_post_hook_A2(val):
        
        yield from bps.mov(motorB, val*2)
        yield from bps.mov(motorC, val*2)

    def example_pre_hook_B(val):
        
        yield from bps.mov(motorA, val*4)
        yield from bps.mov(motorC, val*4)

    def example_post_hook_B(val):
        
        yield from bps.mov(motorA, val/2)
        yield from bps.mov(motorC, val/2)

    #Test for no hooks added if it's not the correct motor
    hookA  = MotionHook('hookA',motorA,example_pre_hook_A2,example_post_hook_A2)
    hookB  = MotionHook('hookB',motorB,example_pre_hook_B,example_post_hook_B)
    mh = MotionHooksPreprocessor(hooks=[hookA, hookB])
    
    processed = mh(bps.mov(motorA, 12))
    # 2 for the first set and wait, + 8 for pre and post on A. plus 2x8 for each instance of B
    assert len(list(processed)) == 26


def test_motion_hooks_preprocessor_multiple_with_re():
    # Test that I can add multiple hooks that will be run in order

    def example_pre_hook_A2(val):
        
        yield from bps.mov(motorB, val/2)
        yield from bps.mov(motorC, val/2)

    def example_post_hook_A2(val):
        
        yield from bps.mov(motorB, val/4)
        yield from bps.mov(motorC, val/4)

    def example_pre_hook_B(val):
        
        yield from bps.mov(motorA, val*2)
        yield from bps.mov(motorC, val*2)

    def example_post_hook_B(val):
        yield from bps.mov(motorA, val*2)
        yield from bps.mov(motorC, val*4)

    hookA  = MotionHook('hookA',motorA,example_pre_hook_A2,example_post_hook_A2)
    hookB  = MotionHook('hookB',motorB,example_pre_hook_B,example_post_hook_B)
    mh = MotionHooksPreprocessor(hooks=[hookA, hookB])
    RE = RunEngine({})
    RE.preprocessors.append(mh)
    RE(bps.mov(motorB, 10))

    
    assert motorA.read()['motorA']['value'] == 20.0
    assert motorB.read()['motorB']['value'] == 10.0
    assert motorC.read()['motorC']['value'] == 40.0

