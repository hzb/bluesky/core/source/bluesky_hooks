import sys
from os import path

from setuptools import find_packages, setup

import versioneer

setup(name='bluesky_hooks',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='Preprocesors to inject plans before and after motor moves',
      url='https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bluesky_hooks',
      author='Will Smith',
      author_email='william.smith@helmholtz-berlin.de',
      # license='MIT',
      packages=find_packages(exclude=['docs', 'tests']),
      install_requires=[
          
          'bluesky>=1.10.0'
        
      ]
      # zip_safe=False
)
